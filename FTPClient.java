/**
 * Created by ben on 9/28/16.
 */
// FTP Client

import java.net.*;
import java.io.*;

class FTPClient {
    public static void main(String args[]) throws Exception {
        Socket soc = new Socket("127.0.0.1", 5217);
        TransferFileClient t = new TransferFileClient(soc);
        t.displayMenu();
    }
}
